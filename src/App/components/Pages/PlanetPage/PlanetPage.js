import React from 'react';
import styles from './PlanetPage.module.css'

//import components 
import CardHolder from '../../components/CardHolder/CardHolder';

const api_key = "f7311b9ba93800529c82af377518c9c76da2b9c55f424609dfbe30104b815e3a";
class PlanetPage extends React.Component {

    constructor(props) {
        super(props);

        this.updateStateTemp  = this.updateStateTemp.bind(this);
        this.handleEditPlanet = this.handleEditPlanet.bind(this);
    }

    state = {
        planets: [],
        showEdit: false,
    };


    async componentDidMount() {


        try {
            const resp = await fetch('https://planetsapi-noroff.herokuapp.com/api/v1/planets', {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${api_key}`,
                }
            }).then(resp => resp.json());
            this.setState({
                planets: resp.slice()
            });
            //console.log(resp);
            //console.log(this.state.planets);
        } catch(err) {
            console.error(err);
        }
    }

    updateState(id) {
        this.setState({
            planets: this.state.planets.filter((planet) => planet.id !== id )
        })
    }

    updateStateTemp(planetObject) {
        //console.log(this);
        this.setState({
            planets: this.state.planets.map((planet) => {
                if(planet.id === planetObject.id) {
                    //console.log("oppdaterer");
                    //console.log(planetObject);
                    return planetObject;
                } else {
                    //console.log("Oppdaterer ikke")
                    //console.log(planet);
                    return planet;
                }
            })
        }) 
    }

    async handleOnClick(id) { 
        try{
            const resp = await fetch(`https://planetsapi-noroff.herokuapp.com/api/v1/planets/delete?id=${id}`, {
                method: 'DELETE',
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${api_key}`,
                }
            }).then(resp => resp.json());
            this.updateState(id);
            //console.log(resp);
        } catch(err) {
            //console.error(err);
        }
    }

    async handleEditPlanet(planetObject) {
        try {
            const resp = await fetch("https://planetsapi-noroff.herokuapp.com/api/v1/planets/update", {
                method: 'PATCH',
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${api_key}`
                }, 
                body: JSON.stringify({
                    planet: {
                        id: planetObject.id,
                        name: planetObject.name,
                        size: planetObject.size,
                        date_discovered: planetObject.date_discovered
                    }
                })
            }).then(resp => resp.json());
            //console.log(resp.planet);
            //console.log("Kommer hit 1");
        } catch(err) {
            console.error(err);
        }
     //   this.updateStateTemp(resp.planet);
    }

    render() {

        const planets = this.state.planets.map(planet => {
            return (
                <div>
                    <CardHolder planet={planet} key={planet.id} update={this.updateStateTemp} onClick={() => this.handleOnClick(planet.id)} onClickEditPlanet={(planetObject) => this.handleEditPlanet(planetObject)}></CardHolder>
                </div>
            );
        })

        return (
            <div className="container">
                <h1>PlanetList</h1>
                <hr/>
                <div className={styles.Cards}> 
                    {planets}
                </div>
            </div>
        );
    }
}

export default PlanetPage;