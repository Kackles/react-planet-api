import React from 'react';
import UpdateForm from '../../components/UpdateForm/UpdateForm';

class AddPlanetPage extends React.Component {

    constructor(props) {
        super(props);
        this.onSubmit=this.handlePlanetSubmit.bind(this);
    }

    state = {
        showMessage: false,
        whatMessage: false,
    }

    async handlePlanetSubmit(childState) {
        //e.preventDefault();
        //console.log(this.refs.name.value);
        console.log("kom hit");
        console.log(childState);

        try {
            const resp = await fetch('https://planetsapi-noroff.herokuapp.com/api/v1/planets/add', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer f7311b9ba93800529c82af377518c9c76da2b9c55f424609dfbe30104b815e3a',
                },
                body: JSON.stringify({
                    planet: {
                        name: childState.name,
                        size: childState.size,
                        date_discovered: childState.date_discovered
                    }
                })
            }).then(resp => resp.json());
            console.log(resp);
            if(resp.success === false) {
                alert("Something went wrong, could not add planet");
            } else {
                alert("Planet was added to database");
            }    
        } catch(err) {
            alert(err.message);
            console.error(err);
        }

    }


    render() {
        return (
                <div>
                    <UpdateForm onHandleSubmit={this.handlePlanetSubmit}></UpdateForm>
                    {this.state.showMessage === true && ((this.state.whatMessage === true && <h1>Planet was added to database</h1>) || (this.state.whatMEssage === false && <h1>Something went wrong, planet was not added to the database</h1>))}
                </div>
        );
    }
}

export default AddPlanetPage;