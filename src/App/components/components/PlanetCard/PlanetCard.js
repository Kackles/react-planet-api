import React from 'react';

function PlanetCard(props) {
    return (
        <div>
            <hr/>
            <h4> {props.planet.id} </h4>
            <h4> {props.planet.name}</h4>
            <h4> {props.planet.size} </h4>
            <h4> {props.planet.date_discovered}</h4> 
            <button onClick={props.onClick}>Delete</button>
            <button onClick={props.onClickEdit}>Edit</button>
            <hr/>                   
            <br/>
        </div>
    );
}

export default PlanetCard