import React from 'react';

//import styles
import styles from './CardHolder.module.css';

//import components
import PlanetCard from '../PlanetCard/PlanetCard';
import UpdateForm from '../UpdateForm/UpdateForm';

const api_key = "f7311b9ba93800529c82af377518c9c76da2b9c55f424609dfbe30104b815e3a";

class CardHolder extends React.Component {

    constructor(props) {
        super(props);

        this.handleOnClickEdit = this.handleOnClickEdit.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
    }

    state = {
        planet: this.props.planet,
        showEdit: false,
    }

    handleOnClickEdit() {
        //console.log(this);
        this.setState({
            showEdit: !this.state.showEdit,
        })
    }

    updateState(resp) {
        //console.log(resp);
        this.setState({
            planet: resp,
            showEdit: !this.state.showEdit,
        })
    }

    //this function is just there to add the id to the planetObject
    async handleEdit(planetObject) {
        try {
            const resp = await fetch("https://planetsapi-noroff.herokuapp.com/api/v1/planets/update", {
                method: 'PATCH',
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${api_key}`
                }, 
                body: JSON.stringify({
                    planet: {
                        id: this.state.planet.id,
                        name: planetObject.name,
                        size: planetObject.size,
                        date_discovered: planetObject.date_discovered
                    }
                })
            }).then(resp => resp.json());
            //console.log(resp.planet);
            //console.log("Kommer hit 1");
            this.updateState(resp.planet);
        } catch(err) {
            console.error(err);
        }
    }

    render() {
        return (
            <div className={styles.Card}>
                <PlanetCard planet={this.state.planet} onClick={this.props.onClick} onClickEdit={this.handleOnClickEdit}></PlanetCard>
                {this.state.showEdit === true && 
                    <UpdateForm onHandleSubmit={this.handleEdit}></UpdateForm>
                }
            </div>
        );
    }
}

export default CardHolder;