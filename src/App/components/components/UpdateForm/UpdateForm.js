import React from 'react';

class UpdateForm extends React.Component {
    
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    state = {
        name: '',
        size: '',
        date_discovered: '',
    }


    handleChange(event) {
        event.preventDefault();
        const name = event.target.name;
       this.setState({
        [name]: event.target.value,
        });
        //console.log(event);
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.onHandleSubmit({
            id: '',
            name: this.state.name,
            size: this.state.size,
            date_discovered: this.state.date_discovered,
        });
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <input type="text" name="name" value={this.state.name} onChange={this.handleChange} placeholder="Add name"/>
                        <input type="text" name="size" value={this.state.size} onChange={this.handleChange} placeholder="Add a size"/>
                    </div>
                    <div>
                        <input type="text" name="date_discovered" value={this.state.date_discovered} onChange={this.handleChange} placeholder="Add a discovery date"/>
                        <input type="submit" value="submit" />
                    </div>
                </form>
            </div>
        );
    }
}

export default UpdateForm;