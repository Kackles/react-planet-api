import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'

//add some style
import './App.css';

//add components
import PlanetPage from './components/Pages/PlanetPage/PlanetPage';
import AddPlanetPage from './components/Pages/AddPlanetPage/AddPlanetPage';
import UpdatePlanetPage from './components/Pages/UpdatePlanetPage/UpdatePlanetPage';
import DeletePlanetPage from './components/Pages/DeletePlanetPage/DeletePlanetPage';

//create a state-full component
class App extends React.Component {
    

    render() {
        return (
            <React.Fragment>
            <h1 className="App">Planet Api</h1>
            <Router>
                <ul>
                    <li>
                        <Link to="/">Planets</Link>
                    </li>
                    <li>
                        <Link to="/addPlanet">Add a Planet</Link>
                    </li>
                </ul>
                <hr/>
                <Route exact path="/" component={PlanetPage}></Route>
                <Route path="/addPlanet" component={AddPlanetPage}></Route>
            </Router>  
            </React.Fragment>
        );
    }
}

export default App;